var express = require("express");
var path = require('path');
var fs = require('fs');
var conf = require('config');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var session = require('express-session');
var url = require('url');
var sprintf = require("sprintf-js").sprintf;
var httpProxy = require("http-proxy");
var http = require('http');
var fs = require('fs');
var filename = 'target.json';

global.appRoot = path.resolve(__dirname);

var PORT = conf.get('port');
var app = express();

console.log(sprintf("using env: [%s]", app.get('env')));
app.set('view engine', 'html') ;
app.use(express.static(path.join(__dirname, 'public')));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(session({
    secret : "123456",
    saveUninitialized: true,
    resave : false
}));
 var x;


app.use("/try", function(req, res){
    var httpProxy = require('http-proxy');
    var proxy = httpProxy.createServer(function (req, res, proxy) {
      proxy.proxyRequest(req, res, {host: 'http://localhost:9000'});
    })

    proxy.listen(8000);
});


var tar = require('./routes/seturl');
app.use(tar);

var temp = require('./routes/forward');
app.use(temp);


if (app.get('env') === 'development') {
    app.use(function(err, req, res, next) {
        console.log("\n" + err);
    });
}

app.use(function(err, req, res, next) {
    console.log("\n" + err);
});

app.listen(PORT) ;
console.log(sprintf('Listening on port %s...', PORT));
