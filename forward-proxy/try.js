var express = require("express");
var path = require('path');
var fs = require('fs');
var conf = require('config');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var session = require('express-session');
var url = require('url');
var sprintf = require("sprintf-js").sprintf;
var httpProxy = require("http-proxy");
var http = require('http');
var fs = require('fs');
var filename = 'target.json';

var httpProxy = require('http-proxy');
var proxy = httpProxy.createProxyServer({
    autoRewrite: true,
    hostRewrite: true/*,
    xfwd: true,*/
});

var app = express();

console.log(sprintf("using env: [%s]", app.get('env')));
app.set('view engine', 'html') ;
app.use(express.static(path.join(__dirname, 'public')));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(session({
    secret : "123456",
    saveUninitialized: true,
    resave : false
}));

proxy.on('error', function(e) {
  console.log("proxy error", e);
});

proxy.on('proxyRes', function (proxyRes, req, res) {
  console.log('RAW request headers ', JSON.stringify(req.headers, true, 2));
  console.log('RAW Response from the target', JSON.stringify(proxyRes.headers, true, 2));
});





app.get("/", function(req, res){
//    req.headers["host"] = "requestb.in"

    proxy.web(req, res, {
        target: "http://requestb.in/1h6nf291",
        changeOrigin: true,
        autoRewrite:true,
        ignorePath: true
    }, function(e) {
            if(e) {
                console.log("Error.....", e);
                throw e;
            }

            console.log("proxying request...");
            res.end("Done proxying.");
    });
});

app.listen(4444) ;
console.log(sprintf('Listening on port %s...', 4444));
