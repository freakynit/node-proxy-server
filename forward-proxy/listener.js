var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var session = require('express-session');
var express = require("express");
var http = require('http');
var url = require('url');

var app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(session({
    secret : "123456",
    saveUninitialized: true,
    resave : false
}));

app.use(function(req, res){
    console.log("received hit");
    res.json({
        "headers": JSON.stringify(req.headers, true, 2),
        "query-params": url.parse(req.url).query ? JSON.stringify( url.parse(req.url).query) : ""
    }).send();
});

app.listen(9000);
console.log('Listening on port %s...' + 9000);
