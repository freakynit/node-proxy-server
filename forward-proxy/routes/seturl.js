var express = require('express');
var router = express.Router();
var conf = require('config');
var request = require('request');
var querystring = require('querystring');
var url = require('url');
var sprintf = require("sprintf-js").sprintf;
var fs = require('fs');
var filename = 'target.json';

router.get('/settarget', function(req, res) {

    var rawQueryString = url.parse(req.url).query;
    var queryParams = querystring.parse(rawQueryString);
    console.log(sprintf("received payload: [%s]", decodeURIComponent(rawQueryString)));
    var set = queryParams.target;
    console.log(set);
    var file_content = fs.readFileSync(filename);
    var content = JSON.parse(file_content);
    content.url = set;
     fs.writeFileSync(filename, JSON.stringify(content), 'utf8');
    var content = fs.readFileSync(filename);
        content = JSON.parse(content);
        console.log("OBJ : " + content.url);
    res.status(200).json({"url" : content.url}).end();
});


// Module Exports
module.exports = router;
