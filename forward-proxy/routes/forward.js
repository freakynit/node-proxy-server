var express = require('express');
var router = express.Router();
var conf = require('config');
var request = require('request');
var querystring = require('querystring');
var url = require('url');
var sprintf = require("sprintf-js").sprintf;
var fs = require('fs');
var filename = 'target.json';
var httpProxy = require("http-proxy");
var http = require('http');

 var apiProxy = httpProxy.createProxyServer();
// var prox = function createProxyServer(options) {
//   autoRewrite: true;
//   return new httpProxy.Server(options);
// };

router.all('/', function (req, res, options){
    var rawQueryString = url.parse(req.url).query;
    var queryParams = querystring.parse(rawQueryString);
    var contents = fs.readFileSync(filename);
    contents = JSON.parse(contents);
    console.log("OBJ : " + contents.url);
    var proxyurl = contents.url+"?"+rawQueryString;
    apiProxy.web(req, res, {
                            target: proxyurl,
                            changeOrigin: true,
                            ignorePath: true

                           });
});

//1234

module.exports = router;
