var TARGET_ADDRESS_FILE = './targetAddress.txt';
var PROXY_PORT = 1912;

var querystring = require('querystring');
var url = require('url');
var http = require('http');
var https = require('https');
var httpProxy = require('http-proxy');
var fs = require('fs');
var express = require('express');

var cl = function(msg){
	console.log(msg);
};

// Create http agent to accept any certificates
var options = {
 	HOST: 'webengage.com',
  	checkServerIdentity: function ( HOST, cert) {
    	return undefined;
  	}
};
var httpAgent = new http.Agent(options);

// Create proxy server
var proxy = httpProxy.createProxyServer({/*target:'http://requestb.in/1l7tb5r1'*/ "agent": httpAgent, "secure": false, 'changeOrigin': true,"ignorePath":true})/*.listen(PROXY_PORT)*/;
proxy.on('proxyRes', function (proxyRes, req, res) {
  console.log('RAW Response from the target', JSON.stringify(proxyRes.headers, true, 2));
});
proxy.on('open', function (proxySocket) {
  // listen for messages coming FROM the target here
  proxySocket.on('data', hybiParseAndLogMessage);
});
proxy.on('close', function (res, socket, head) {
  // view disconnected websocket connections
  console.log('Client disconnected');
});
proxy.on('error', function (error, req, res) {
    var json;
    console.log('proxy error', error);
    if (!res.headersSent) {
        res.writeHead(500, { 'content-type': 'application/json' });
    }

    json = { error: 'proxy_error', reason: error.message };
    res.end(JSON.stringify(json));
});


// Start main server
var proxyurl;
var server = http.createServer(function (req, res) {
  console.log(JSON.stringify(req.headers, true, 2));
  var rawQueryString = url.parse(req.url).query;
    var queryParams = querystring.parse(rawQueryString);
  var targetAddress = fs.readFileSync(TARGET_ADDRESS_FILE, 'utf-8');
  if(rawQueryString !== null)
    proxyurl = targetAddress+"?"+rawQueryString;
  else
    proxyurl = targetAddress;
  console.log("targetAddress = " + proxyurl);
  
  proxy.web(req, res, {target: proxyurl/*'http://requestb.in/1lteic71'*//*'http://localhost:3345'*//*'https://c.webengage.com/m1.jpg'*/, "agent": httpAgent, "secure": false}, function(error){
  	if(error) console.log('proxy error 2', error);
  });
});

console.log("main server listening on port " + PROXY_PORT)
server.listen(PROXY_PORT);
