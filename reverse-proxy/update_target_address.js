var TARGET_ADDRESS_FILE = './targetAddress.txt';

var express = require('express');
var fs = require('fs');

var app = express();

app.get('/update', function(req, res){
  if(req.query.targetAddress) {
    var targetAddress = req.query.targetAddress;
    fs.writeFileSync(TARGET_ADDRESS_FILE, targetAddress, 'utf-8');
    res.send("targetAddress set to: " + targetAddress);
  } else {
    res.send("Please send targetAddress as query parameter");
  }
});

app.get('/ping', function(req, res){
	res.send('pong');
});

app.listen(3345);
